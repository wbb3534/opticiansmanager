package com.seop.opticiansmanager.controller;

import com.seop.opticiansmanager.model.CustomerItem;
import com.seop.opticiansmanager.model.CustomerRequest;
import com.seop.opticiansmanager.model.CustomerResponse;
import com.seop.opticiansmanager.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/customer")
@RequiredArgsConstructor
public class CustomerController {
    private final CustomerService customerService;

    @PostMapping("/data")
    public String setCustomerData(@RequestBody @Valid CustomerRequest request) {
        customerService.setCustomerData(request);
        return "성공";
    }
    @GetMapping("/all")
    public List<CustomerItem> getCustomers() {
        List<CustomerItem> result = customerService.getCustomers();
        return result;
    }

    @GetMapping("/data/id/{id}")
    public CustomerResponse getCustomer(@PathVariable long id) {
        CustomerResponse result = customerService.getCustomer(id);
        return result;
    }
}
