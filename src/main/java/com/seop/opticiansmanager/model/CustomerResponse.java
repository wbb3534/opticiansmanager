package com.seop.opticiansmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CustomerResponse {
    private Long id;
    private String customerName;
    private LocalDate dateLast;
    private String figureEye;
    private String needGlassesRight;
    private String needGlassesLeft;
    private Boolean superEyes;
}
