package com.seop.opticiansmanager.repository;

import com.seop.opticiansmanager.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
