package com.seop.opticiansmanager.service;

import com.seop.opticiansmanager.entity.Customer;
import com.seop.opticiansmanager.model.CustomerItem;
import com.seop.opticiansmanager.model.CustomerRequest;
import com.seop.opticiansmanager.model.CustomerResponse;
import com.seop.opticiansmanager.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    public void setCustomerData(CustomerRequest request) {
        Customer addData = new Customer();
        addData.setCustomerName(request.getCustomerName());
        addData.setCustomerPhone(request.getCustomerPhone());
        addData.setFigureEyeLeft(addData.getFigureEyeLeft());
        addData.setFigureEyeRight(request.getFigureEyeRight());
        addData.setDateLast(LocalDate.now());

        customerRepository.save(addData);
    }

    public List<CustomerItem> getCustomers() {
        List<Customer> originData = customerRepository.findAll();

        List<CustomerItem> result = new LinkedList<>();

        for (Customer item : originData) {
            CustomerItem addItem = new CustomerItem();
            addItem.setId(item.getId());
            addItem.setCustomerName(item.getCustomerName());
            addItem.setCustomerPhone(item.getCustomerPhone());
            addItem.setDateLast(item.getDateLast());

            result.add(addItem);
        }
        return result;
    }
    public CustomerResponse getCustomer(long id) {
        Customer originData = customerRepository.findById(id).orElseThrow(); //orElseThrow() 없으면 버린다

        CustomerResponse result = new CustomerResponse();
        result.setId(originData.getId());
        result.setCustomerName(originData.getCustomerName());
        result.setDateLast(originData.getDateLast());
        result.setFigureEye("좌" + originData.getFigureEyeRight() + "/" + "우" + originData.getFigureEyeLeft());
        result.setNeedGlassesRight(originData.getFigureEyeRight() < 1.0 ? "안경필요" : "안경불필요");
        result.setNeedGlassesLeft(originData.getFigureEyeLeft() < 1.0 ? "안경필요" : "안경불필요");
        result.setSuperEyes((originData.getFigureEyeRight() >= 1.5 && originData.getFigureEyeLeft() >= 1.5) ? true : false);

        return result;
    }
}
